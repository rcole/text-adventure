import { DeepstreamClient } from "@deepstream/client";

const dsClient = new DeepstreamClient("localhost:6020");

export const doLogin = loginParams => {
  return dsClient.login(loginParams, (success, data) => {
    console.log(success, data);
  });
};

export const initConnectionListener = callback => {
  callback(dsClient.getConnectionState());
  dsClient.on("connectionStateChanged", connectionState => {
    callback(connectionState);
  });
};

// export const getLoginState = () => {
//   return dsClient.login();
// };

export const emitEvent = (topic, data) => {
  return dsClient.event.emit(topic, data);
};

export const subscribeEvent = (topic, handler) => {
  return dsClient.event.subscribe(topic, handler);
};

export const unsubscribeEvent = (topic, handler) => {
  return dsClient.event.subscribe(topic, handler);
};

export const subscribePresence = handler => {
  return dsClient.presence.subscribe(handler);
};

export const unsubscribePresence = handler => {
  return dsClient.presence.subscribe(handler);
};

export const getAllConnectedUsers = handler => {
  return dsClient.presence.getAll(handler);
};
