/* eslint-disable class-methods-use-this */
/* eslint-disable import/prefer-default-export */
import G from "./game/index";
import { dimensions, definitions } from "./gameData/places";
import descriptions from "./gameData/placeDescriptions";
import { things } from "./gameData/things";
import { themes } from "./gameData/colorThemes";

export class GameWrapper {
  constructor(store, deepStream) {
    this.$store = store;
    this.game = null;
    this.deepStream = deepStream;
  }

  playerNearHandler(cell, player) {
    console.log("playerNearHandler", cell, player);
  }

  playerGoneHandler(cell, player) {
    console.log("playerGoneHandler", cell, player);
  }

  addMoveListener() {
    this.deepStream.subscribeEvent("player/move", event => {
      this.$store.dispatch("map/updatePlayersInArea", event);
    });
  }

  addChatListener() {
    this.deepStream.subscribeEvent("player/say", event => {
      console.log(
        "caught say",
        event,
        this.game.currentPosition,
        this.game.playerName,
        event.area,
        event.area.includes(this.game.currentPosition),
      );
      if (event.player !== this.game.playerName) {
        if (event.area.includes(this.game.currentPosition)) {
          this.$store.dispatch("messages/addMessage", {
            ...event,
            success: true,
            source: "playerChat",
          });
        }
      }
    });
  }

  initGame(playerName) {
    this.game = G({
      playerName,
      placesData: { dimensions, descriptions, definitions },
      actorsData: {},
      thingsData: things,
      startPosition: "a1",
      themes,
    });

    this.game.addResponseHandler(resp => {
      if (resp.message !== undefined) {
        this.$store.dispatch("messages/addMessage", resp);
      }
    });

    this.game.addChatHandler(resp => {
      if (resp.message !== undefined) {
        this.$store.dispatch("messages/addPlayerMessage", resp);

        console.log({
          pos: this.$store.state.map.currentPosition,
          area: this.$store.state.map.currentArea,
          player: playerName,
          message: resp.message,
        });

        this.deepStream.emitEvent(
          "player/say",
          this.game.addMessageId({
            pos: this.$store.state.map.currentPosition,
            area: this.$store.state.map.currentArea,
            player: playerName,
            message: resp.message,
          })
        );
      }
    });

    this.game.addThemeHandler(theme => {
      this.$store.dispatch("map/setCurrentTheme", theme);
    });

    this.game.addMoveHandler(({ to, from }) => {
      const adjTo = this.game.getAdjacent(to);
      this.$store.dispatch("map/setCurrentPosition", to);
      this.$store.dispatch("map/setCurrentArea", adjTo);
      this.deepStream.emitEvent("player/move", {
        player: playerName,
        to,
        from,
      });
    });
  }

  sendMessage(message) {
    this.game.parseText(message);
  }
}
