/* eslint-disable import/prefer-default-export */
import Action from "./Action";

export const buildActions = function(actionData) {
  return actionData.map(d => new Action(d));
};
