module.exports = {
  // prettier/vue disables stylistic rules from vue rules - required to avoid conflicts
  root: true,
  extends: ["plugin:nuxt/recommended", "@genster/eslint-config"],
  env: { jest: true, browser: true, node: true },
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {
    "no-param-reassign": "off",
    "comma-dangle": [2, "always-multiline"],
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "vue/no-deprecated-filter": "off",
    "vue/no-deprecated-v-bind-sync": "off",
    "vue/no-deprecated-slot-attribute": "off",
  },
};
