export const state = () => ({
  connectionState: null,
  loginState: null,
});

export const actions = {
  setConnectionState({ commit }, connectionState) {
    commit("SETCONNECTIONSTATE", connectionState);
  },
  setLoginState({ commit }, loginState) {
    commit("SETLOGINSTATE", loginState);
  },
};

export const mutations = {
  SETCONNECTIONSTATE($state, connectionState) {
    $state.connectionState = connectionState;
  },
  SETLOGINSTATE($state, loginState) {
    $state.loginState = loginState;
  },
};
